import { IsNotEmpty, IsNumber, IsString, MaxLength } from 'class-validator';
export class CreateUserDto {
  @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  readonly email: string;

  @IsString()
  @IsNotEmpty()
  readonly password: string;

  @MaxLength(20)
  @IsNotEmpty()
  @IsNumber()
  readonly phone_no: number;

  @IsString()
  @IsNotEmpty()
  readonly address: string;
}
