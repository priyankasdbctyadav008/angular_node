export class User {
  name: string;
  email: string;
  password: string;
  phone_no?: number;
  address?: string;
}
