/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
    'node_modules/preline/dist/*.js',
  ],
  theme: {
    extend: {
      fontFamily: {
        custom: 'var(--font-family)',
      },
      fontSize: {
        'h1': '1.5rem', // Equivalent to 24px
        'h2': '1.25rem', // Equivalent to 20px
        'h3': '1.125rem', // Equivalent to 18px
        'h4': '1.125rem', // Equivalent to 18px
        'h5': '1.125rem', // Equivalent to 18px
        'h6': '1.125rem', // Equivalent to 18px
        'body1': '1rem', // Equivalent to 16px
        'body2': '0.875rem', // Equivalent to 14px
      },
      fontWeight: {
        'normal': 400,
        'medium': 500,
        'bold': 700,
      },
      colors: {
        primary: {
          main: '#125893',
          light: '#e5f6ff',
        },
        secondary: {
          main: '#D7F1FF',
          light: '#F7F7F7',
        },
        common: {
          body: '#f4f5f7',
        },
        text: {
          dark: 'rgb(0 0 0 / 80%)',
          secondary: '#7E7E7E',
          main: '#125893',
        },
      },
      boxShadow: {
        '4': '0 2px 5px rgb(0 0 0 / 7%)',
        '5': '4px 4px 8px rgba(0, 0, 0, 0.09)',
      },
    },
  },
  plugins: [require('preline/plugin'),],
}

